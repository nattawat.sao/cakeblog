<!-- File: /app/View/Posts/index.ctp (edit links added) -->
<!-- for admin -->
<br>
<?php if($current_user['role'] == 'admin'):?>
<div class='container'>
    <div class='row'>
        <div class='col-sm-12'>
            <div class="card">
                <div class="card-header">
                    <h4 class="text-dark">Contents</h4>
                </div>
                <div class="card-body">
                    <a href='http://localhost/cakeblog/Posts/add'><button class="block" align="center">Create your
                            content</button></a><br>
                    <table>
                        <tr>
                            <th>Id</th>
                            <th>Title</th>
                            <th>Action</th>
                            <th>Created</th>
                            <th>Created By</th>
                            <!-- <th>Creared By</th> -->
                        </tr>
                        <!-- Here's where we loop through our $posts array, printing out post info -->
                        <?php foreach ($posts as $post): ?>
                        <tr>
                            <td><?php echo $post['Post']['id']; ?></td>
                            <td>
                                <?php
                                    echo $this->Html->link(
                                    $post['Post']['title'],
                                    array('action' => 'view', $post['Post']['id']),
                                    array('style' => 'color: black;')
                                    );
                                ?>
                            </td>
                            <td>
                                <?php if($current_user['id'] == $post['User']['id'] || $current_user['role'] == 'admin'):?>
                                <?php
                                    echo $this->Html->link
                                    (
                                        'Edit',
                                        array('action' => 'edit', $post['Post']['id']),
                                        array('style' => 'color: black;')
                                    );
                                ?>&nbsp;&nbsp;
                                <?php
                                     echo $this->Html->link
                                     (
                                         'Delete',
                                         array('controller' => 'posts','action' => 'delete', $post['Post']['id']),
                                         array('style' => 'color: black;'),
                                         array('confirm' => 'Are you sure')
                                     );
                                ?>
                            </td>
                            <?php endif; ?>
                            <td>
                                <?php echo $post['Post']['created']; ?>
                            </td>
                            <td>
                                <?php echo $this->Html->link(
                                        $post['User']['name'],
                                        array('controller' => 'users','action' => 'view', $post['User']['id']),
                                        array('style' => 'color: black;'));
                                ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif;?>
<!-- for enduser -->
<?php if($current_user['role'] != 'admin'):?>
<div class='container'>
    <div class='row'>
        <div class='col-sm-2'>
        </div>
        <div class='col-sm-8'>
            <div class="card">
                <?php echo $this->Form->create(false, 
                        array('type' => 'file','url' => array('controller'=>'posts', 'action'=>'add'),
                        'id' => 'comment','style' => "width: 100%;")); ?>
                <div class="card-header">
                    <h4 class="text-dark">Write your new content</h4>
                </div>
                <div class="card-body">
                    
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Title</span>
                        </div>
                        <input name="data[Post][title]" class="form-control" maxlength="50" type="text" id="PostTitle"
                            required="required" placeholder="Enter your title">
                    </div>
                    <div class="card">
                        <div class="card-header"
                            style="padding:0.25rem 1.25rem; background-color:#e9ecef; color:#495057; text-align:center;">
                            Write the content</div>
                        <div class="card-body" style="padding:0.25rem;">
                            <textarea name="data[Post][body]" class="form-control" rows="3" cols="30" id="PostBody"
                                required="required" placeholder="write somthing..."></textarea>
                            <div class="custom-file-upload">
                                <input type="file" style="padding: 1% 0% 1% 0%;" name="data[Post][image_name][]"
                                    id="imagePost" multiple="multiple">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer"><button type="submit" class="block">Share</button></div>
                 <?php echo $this->Form->end(); ?>
            </div>
           
        </div>
    </div>
</div>
<br>
<div class='container'>
    <div class="row">
        <div class='col-sm-2'>
        </div>
        <!-- <div class = 'col-sm-8'>
    <a href = 'http://localhost/cakeblog/Posts/add'><button class="block" align="center">Create your content</button></a><br>
</div> -->
    </div>
</div>
<div class='container'>
    <?php $image_num = 0;?>
    <?php foreach ($posts as $post): ?>
    <div class='row'>
        <div class='col-sm-2'>
        </div>
        <div class='col-sm-8'>
            <div class="card" align="left" style="border: 2px solid rgba(0, 0, 0, 0.4);">
                <div class="card-header" style="background-color: #b1b1b1;">
                    <?php echo $this->Html->link(
                                $post['User']['name'],
                                array('controller' => 'users','action' => 'view', $post['User']['id']),
                                array('style' => 'color: black;'));
                            ?>
                </div>
                <div class="card-body">
                    <h5><b><?php
                                    echo $this->Html->link(
                                    $post['Post']['title'],
                                    array('action' => 'view', $post['Post']['id']),
                                    array('style' => 'color: black;')
                                    );
                                    ?></b></h5>
                    <div class="card">
                        <div class="card-footer">
                            <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo h($post['Post']['body']); ?></p>
                            <?php foreach($post['Image'] as $image): ?>
                            <?php if($image['name'] != null):?>
                            <div class="card">
                                <div class="card-boby img-resize" style="padding: 0px 10px 0px 0px; margin: 5px;">
                                    <a data-target="#modalIMG<?php echo $image_num;?>" data-toggle="modal" href="#"
                                        class="color-gray-darker td-hover-none">
                                        <img id="myImg"
                                            src="<?php echo $this->webroot; ?>img/upload/<?php echo $post['Post']['id']?>/<?php echo $image['name'] ;?>"
                                            width='100%'>
                                    </a>
                                </div>
                            </div>
                            <div aria-hidden="true" aria-labelledby="myModalLabel" class="modal fade"
                                id="modalIMG<?php echo $image_num;?>" role="dialog" tabindex="-1">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body mb-0 p-0">
                                            <img src="<?php echo $this->webroot; ?>img/upload/<?php echo $post['Post']['id']?>/<?php echo $image['name'] ;?>"
                                                alt="" style="width:100%;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php $image_num = $image_num + 1;?>
                            <?php endif; ?>
                            <?php endforeach;?>
                        </div>
                    </div>
                </div>
                <div class="card-footer"
                    style="border-top: 0px solid rgba(0,0,0,.125); background-color: rgba(64, 60, 60, 0.05);">comment
                    <div class="input-group mb-6">
                        <input name="comment" class="form-control commentPost" placeholder="write somthing..."
                            type="text" id="commentPost-<?php echo $post['Post']['id']; ?>" required="required">
                        <?php echo $this->Form->input('id', array('type' => 'hidden', 'value' => $post['Post']['id'])); ?>
                        <div class="input-group-append">
                            <button class="btn btn-secondary submit" id="<?php echo $post['Post']['id']; ?>">
                                Comment
                            </button>
                        </div>
                    </div>
                    <br>
                    <div id="divComment-<?php echo $post['Post']['id']; ?>"></div>
                    <?php foreach ($post['UserComment'] as $comment): ?>

                    <div class='card'>
                        <div class="card-header" style="padding:0.25rem 1rem 0.25rem 1rem;">
                            <?php echo $this->Html->link(
                                            $comment['name'],
                                            array('controller' => 'users','action' => 'view', $comment['id']),
                                            array('style' => 'color: black;'));
                                        ?>
                        </div>
                        <div id="displayComment-<?php echo $post['Post']['id']; ?>" class="card-body"
                            style="padding:0.25rem 1rem 0.25rem 1rem;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <?php echo $comment['Comment']['comment']; ?>
                        </div>
                    </div>
                    <br>
                    <div id="newComment"></div>
                    <?php endforeach; ?>
                </div>
            </div>
            <!-- <?php echo $this->Form->end(); ?> -->
        </div>
    </div><br>
    <?php endforeach; ?>
</div>
<?php endif; ?>

<!-- Custom upload multiple file -->
<style>
.custom-file-upload-hidden {
    display: none;
    visibility: hidden;
    position: absolute;
    left: -9999px;
}

.custom-file-upload {
    display: block;
    width: 100%;
    font-size: 16px;

    //border: 1px solid #fff;
    label {
        display: black;
        margin-bottom: 5px;
    }
}

.file-upload-wrapper {
    position: relative;
    margin-bottom: 5px;
    //border: 1px solid #ccc;
}

.file-upload-input {
    width: 100%;
    color: #A2A6A8;
    font-size: 16px;
    padding: 5px 28px;
    /* border: 1; */
    border: 1px solid #ced4da;
    border-radius: .25rem .25rem 0rem 0rem;
    transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
    background-color: #EBF1F5;
    @include transition(all 0.2s ease-in);
    float: left;

    /* IE 9 Fix */
    &:hover,
    &:focus {
        background-color: darken($file-upload-color, 5);
        outline: none;
    }
}

.file-upload-button {
    width: 100%;
    cursor: pointer;
    display: inline-block;
    color: #495057;
    font-size: 16px;
    text-transform: uppercase;
    padding: 5px 28px;
    /* border: none; */
    border: 1px solid #ced4da;
    border-radius: 0rem 0rem .25rem .25rem;
    transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
    background-color: #CACFD2;
    float: left;
    /* IE 9 Fix */
    @include transition(all 0.2s ease-in);
}

.file-upload-button:hover {
    background-color: #ddd;
}
</style>
<script>
(function($) {
    // Browser supports HTML5 multiple file?
    var multipleSupport = typeof $('<input/>')[0].multiple !== 'undefined',
        isIE = /msie/i.test(navigator.userAgent);

    $.fn.customFile = function() {

        return this.each(function() {

            var $file = $(this).addClass('custom-file-upload-hidden'), // the original file input
                $wrap = $('<div class="file-upload-wrapper">'),
                $input = $('<input type="text" class="file-upload-input" />'),
                // Button that will be used in non-IE browsers
                $button = $('<button type="button" class="file-upload-button">Select Images</button>'),
                // Hack for IE
                $label = $('<label class="file-upload-button" for="' + $file[0].id +
                    '">Select Images</label>');

            // Hide by shifting to the left so we
            // can still trigger events
            $file.css({
                position: 'absolute',
                left: '-9999px'
            });

            $wrap.insertAfter($file)
                .append($file, $input, (isIE ? $label : $button));

            // Prevent focus
            $file.attr('tabIndex', -1);
            $button.attr('tabIndex', -1);

            $button.click(function() {
                $file.focus().click(); // Open dialog
            });

            $file.change(function() {

                var files = [],
                    fileArr, filename;

                // If multiple is supported then extract
                // all filenames from the file array
                if (multipleSupport) {
                    fileArr = $file[0].files;
                    for (var i = 0, len = fileArr.length; i < len; i++) {
                        files.push(fileArr[i].name);
                    }
                    filename = files.join(', ');

                    // If not supported then just take the value
                    // and remove the path to just show the filename
                } else {
                    filename = $file.val().split('\\').pop();
                }

                $input.val(filename) // Set the value
                    .attr('title', filename) // Show filename in title tootlip
                    .focus(); // Regain focus

            });

            $input.on({
                blur: function() {
                    $file.trigger('blur');
                },
                keydown: function(e) {
                    if (e.which === 13) { // Enter
                        if (!isIE) {
                            $file.trigger('click');
                        }
                    } else if (e.which === 8 || e.which === 46) { // Backspace & Del
                        // On some browsers the value is read-only
                        // with this trick we remove the old input and add
                        // a clean clone with all the original events attached
                        $file.replaceWith($file = $file.clone(true));
                        $file.trigger('change');
                        $input.val('');
                    } else if (e.which === 9) { // TAB
                        return;
                    } else { // All other keys
                        return false;
                    }
                }
            });

        });

    };

    // Old browser fallback
    if (!multipleSupport) {
        $(document).on('change', 'input.customfile', function() {

            var $this = $(this),
                // Create a unique ID so we
                // can attach the label to the input
                uniqId = 'customfile_' + (new Date()).getTime(),
                $wrap = $this.parent(),

                // Filter empty input
                $inputs = $wrap.siblings().find('.file-upload-input')
                .filter(function() {
                    return !this.value
                }),

                $file = $('<input type="file" id="' + uniqId + '" name="' + $this.attr('name') + '"/>');

            // 1ms timeout so it runs after all other events
            // that modify the value have triggered
            setTimeout(function() {
                // Add a new input
                if ($this.val()) {
                    // Check for empty fields to prevent
                    // creating new inputs when changing files
                    if (!$inputs.length) {
                        $wrap.after($file);
                        $file.customFile();
                    }
                    // Remove and reorganize inputs
                } else {
                    $inputs.parent().remove();
                    // Move the input so it's always last on the list
                    $wrap.appendTo($wrap.parent());
                    $wrap.find('input').focus();
                }
            }, 1);

        });
    }

}(jQuery));

$('input[type=file]').customFile();
</script>