<!-- File: /app/View/Posts/edit.ctp -->
<br>
<div class='container'>
    <div class='row'>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-8">
            <div class="card">
                <div class="card-header">
                    <h4 class="text-dark">Edit content</h4>
                </div>
                <div class="card-body">
                    <?php
                        echo $this->Form->create('Post',array('type' => 'file'));
                        echo $this->Form->input('title',array('class' => 'form-control'));
                        echo $this->Form->input('body', array('rows' => '3','class' => 'form-control'));
                        echo $this->Form->input('id', array('type' => 'hidden'));
                    ?>
                    <label><b>Image</b></label>
                    <div class = "row">
                        <div class="custom-file-upload col-12">
                            <input type="file" style = "padding: 1% 0% 1% 0%;"name="data[Post][image_name][]" id="imagePost" multiple="multiple">
                    </div>
                    <?php $img_num = 0;?>
                    <?php if($this->request->data['Image'] != null):?>
                    <?php foreach($this->request->data['Image'] as $image):?>
                        <?php if($img_num != 0): ?>
                            <br>
                        <?php endif; ?>
                        <div class = "card" style = "paddign:0rem;" id = "div_ShowImage.<?php echo $image['id'];?>">
                            <div class ="polaroid " style="padding: 5px;">
                            <img src="<?php echo $this->webroot; ?>img/upload/<?php echo $image['post_id']?>/<?php echo $image['name'] ;?>" width='100%'>
                                <!-- <button type="button" class="btn-m-edit btn-secondary show_box_change-image">Change</button> -->
                                <button type="button" 
                                        class="btn-m-delete btn-outline-secondary" 
                                        id ="deleteImage"
                                        onclick="window.location.href='<?php echo $this->Html->url(array('controller' => 'posts','action' => 'deleteImg', $image['id']));?>'"
                                        >Delete</button>
                                <div class = "container-polaroid-t">
                                    <p style = "margin-bottom: 0rem;"><b>Created </b>| <?php echo $image['created']; ?></p>
                                </div>
                            </div>
                        </div>
                        <?php $img_num++;?>
                    <?php endforeach;?>
                    <?php endif;?>
                </div><br>
            <div class="card-footer" 
            style = "padding: 0rem;
                    background-color: rgba(0,0,0,0);
                    border-top: 0;"><button type="submit" class="block">Done</button></div>
            </div>
        </div>
    </div>
</div>
<!-- Custom upload multiple file -->
<style>
    .custom-file-upload-hidden {
    display: none;
    visibility: hidden;
    position: absolute;
    left: -9999px;
    }

    .custom-file-upload {
    display: block;
    width: 100%;
    font-size: 16px;
    //border: 1px solid #fff;
    label {
            display: black;
            margin-bottom: 5px;
        }
    }

    .file-upload-wrapper {
        position: relative; 
        margin-bottom: 5px;
        //border: 1px solid #ccc;
    }
    .file-upload-input {
        width: 100%;
        color: #A2A6A8;
        font-size: 16px;
        padding: 5px 28px;
        /* border: 1; */
        border: 1px solid #ced4da;
        border-radius: .25rem .25rem 0rem 0rem;
        transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
        background-color: #EBF1F5; 
        @include transition(all 0.2s ease-in); 
        float: left; /* IE 9 Fix */
        &:hover, &:focus { 
            background-color: darken($file-upload-color, 5);
            outline: none; 
        }
    }
    .file-upload-button {
        width: 100%;
        cursor: pointer; 
        display: inline-block; 
        color: #495057;
        font-size: 16px;
        text-transform: uppercase;
        padding: 5px 28px;
        /* border: none; */
        border: 1px solid #ced4da;
        border-radius: 0rem 0rem .25rem .25rem;
        transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
        background-color: #CACFD2; 
        margin-bottom: 1rem;        
        float: left; /* IE 9 Fix */
        @include transition(all 0.2s ease-in);

    }
    .file-upload-button:hover 
    {
        background-color: #ddd;
    }
</style>
<script>
    (function($) {
    // Browser supports HTML5 multiple file?
    var multipleSupport = typeof $('<input/>')[0].multiple !== 'undefined',
        isIE = /msie/i.test( navigator.userAgent );

    $.fn.customFile = function() {

    return this.each(function() {

        var $file = $(this).addClass('custom-file-upload-hidden'), // the original file input
            $wrap = $('<div class="file-upload-wrapper">'),
            $input = $('<input type="text" class="file-upload-input" />'),
            // Button that will be used in non-IE browsers
            $button = $('<button type="button" class="file-upload-button">Select Images</button>'),
            // Hack for IE
            $label = $('<label class="file-upload-button" for="'+ $file[0].id +'">Select Images</label>');

        // Hide by shifting to the left so we
        // can still trigger events
        $file.css({
        position: 'absolute',
        left: '-9999px'
        });

        $wrap.insertAfter( $file )
        .append( $file, $input, ( isIE ? $label : $button ) );

        // Prevent focus
        $file.attr('tabIndex', -1);
        $button.attr('tabIndex', -1);

        $button.click(function () {
        $file.focus().click(); // Open dialog
        });

        $file.change(function() {

        var files = [], fileArr, filename;

        // If multiple is supported then extract
        // all filenames from the file array
        if ( multipleSupport ) {
            fileArr = $file[0].files;
            for ( var i = 0, len = fileArr.length; i < len; i++ ) {
            files.push( fileArr[i].name );
            }
            filename = files.join(', ');

        // If not supported then just take the value
        // and remove the path to just show the filename
        } else {
            filename = $file.val().split('\\').pop();
        }

        $input.val( filename ) // Set the value
            .attr('title', filename) // Show filename in title tootlip
            .focus(); // Regain focus

        });

        $input.on({
        blur: function() { $file.trigger('blur'); },
        keydown: function( e ) {
            if ( e.which === 13 ) { // Enter
            if ( !isIE ) { $file.trigger('click'); }
            } else if ( e.which === 8 || e.which === 46 ) { // Backspace & Del
            // On some browsers the value is read-only
            // with this trick we remove the old input and add
            // a clean clone with all the original events attached
            $file.replaceWith( $file = $file.clone( true ) );
            $file.trigger('change');
            $input.val('');
            } else if ( e.which === 9 ){ // TAB
            return;
            } else { // All other keys
            return false;
            }
        }
        });

    });

    };

    // Old browser fallback
    if ( !multipleSupport ) {
    $( document ).on('change', 'input.customfile', function() {

        var $this = $(this),
            // Create a unique ID so we
            // can attach the label to the input
            uniqId = 'customfile_'+ (new Date()).getTime(),
            $wrap = $this.parent(),

            // Filter empty input
            $inputs = $wrap.siblings().find('.file-upload-input')
            .filter(function(){ return !this.value }),

            $file = $('<input type="file" id="'+ uniqId +'" name="'+ $this.attr('name') +'"/>');

        // 1ms timeout so it runs after all other events
        // that modify the value have triggered
        setTimeout(function() {
        // Add a new input
        if ( $this.val() ) {
            // Check for empty fields to prevent
            // creating new inputs when changing files
            if ( !$inputs.length ) {
            $wrap.after( $file );
            $file.customFile();
            }
        // Remove and reorganize inputs
        } else {
            $inputs.parent().remove();
            // Move the input so it's always last on the list
            $wrap.appendTo( $wrap.parent() );
            $wrap.find('input').focus();
        }
        }, 1);

    });
    }

    }(jQuery));

    $('input[type=file]').customFile();
</script>