<!-- File: /app/View/Posts/view.ctp -->
<br>
<div class='container'>
    <div class='row'>
        <div class='col-sm-2'>
        </div>
        <div class='col-sm-8'>
            <div class="card">
                <div class="card-header">
                    <h4 class="text-dark"><?php echo ($post['Post']['title']);?></h4>
                    <?php if($current_user['id'] == $post['User']['id']):?>
                    <?php
                        echo $this->Html->link
                        (
                            'Edit',
                            array('action' => 'edit', $post['Post']['id']),
                            array('style' => 'color: black;')
                        );
                        ?>&nbsp;&nbsp;
                    <?php
                        echo $this->Html->link
                        (
                            'Delete',
                            array('controller' => 'posts','action' => 'delete', $post['Post']['id']),
                            array('style' => 'color: black;'),
                            array('confirm' => 'Are you sure?')
                        );
                    ?>
                    <?php endif;?>
                </div>
                        
                <div class="card-body">
                    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo h($post['Post']['body']); ?></p>
                    <?php $space = 0; 
                          $image_num = 0;
                    ?>
                    <?php foreach($post['Image'] as $image): ?>
                        <?php if($image['name'] != null):?>
                            <?php $space = 1;?>
                            <?php if($image_num >= 1):?>
                                <br>
                            <?php endif?>
                            <div class = "card">
                                <div class ="card-boby" style="padding: 5px;">
                                    <a data-target="#modalIMG<?php echo $image_num;?>" data-toggle="modal" href="#" class="color-gray-darker td-hover-none">
                                        <div class="ba-0 tp-s">
                                            <img id = "myImg" class="card-img-top" src="<?php echo $this->webroot; ?>img/upload/<?php echo $post['Post']['id']?>/<?php echo $image['name'] ;?>" width='100%'>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div aria-hidden="true" aria-labelledby="myModalLabel" class="modal fade" id="modalIMG<?php echo $image_num;?>" role="dialog" tabindex="-1">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body mb-0 p-0">
                                            <img src="<?php echo $this->webroot; ?>img/upload/<?php echo $post['Post']['id']?>/<?php echo $image['name'] ;?>" alt="" style="width:100%">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php $image_num = $image_num + 1;?>
                        <?php endif; ?>
                    <?php endforeach;?>
                    <?php if($space == 0):?>
                        <br><br>
                    <?php endif; ?>
                    <p>
                        <h6 style='text-align:right;'><small>Created: <?php echo $post['Post']['created']; ?></small>
                        </h6>
                        <h6 style='text-align:right;'><small>Created By:
                                <?php echo $this->Html->link(
                                $post['User']['name'],
                                array('controller' => 'users','action' => 'view', $post['User']['id']),
                                array('style' => 'color: gray;'));
                            ?></small></h6>
                    </p>
                </div>
                <div class="card-footer"
                    style="border-top: 0px solid rgba(0,0,0,.125); background-color: rgba(64, 60, 60, 0.05);">comment
                    <div class="input-group mb-6">
                        <input name="comment" class="form-control commentPost" placeholder="write somthing..."
                            type="text" id="commentPost-<?php echo $post['Post']['id']; ?>" required="required">
                        <?php echo $this->Form->input('id', array('type' => 'hidden', 'value' => $post['Post']['id'])); ?>
                        <div class="input-group-append">
                            <button class="btn btn-secondary submit" id="<?php echo $post['Post']['id']; ?>">
                                Comment
                            </button>
                        </div>
                    </div>
                    <br>
                    <div id="divComment-<?php echo $post['Post']['id']; ?>"></div>
                    <?php foreach ($post['UserComment'] as $comment): ?>
                    <div class='card'>
                        <div class="card-header" style="padding:0.25rem 1rem 0.25rem 1rem;">
                            <?php echo $this->Html->link(
                                $comment['name'],
                                array('controller' => 'users','action' => 'view', $comment['id']),
                                array('style' => 'color: black;'));
                            ?>
                        </div>
                        <div id="displayComment-<?php echo $post['Post']['id']; ?>" class="card-body"
                            style="padding:0.25rem 1rem 0.25rem 1rem;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <?php echo $comment['Comment']['comment']; ?>
                        </div>
                    </div>
                    <br>
                    <div id="newComment"></div>
                    <?php endforeach; ?>
                </div>
            </div>
            <!-- <?php echo $this->Form->end(); ?> -->
        </div>
    </div>
</div><br>