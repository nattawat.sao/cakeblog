<!-- File: /app/View/Posts/edit.ctp -->
<br>
<div class='container'>
    <div class='row'>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-8">
            <div class="card">
                <div class="card-header">
                    <h4 class="text-dark">Edit User</h4>
                </div>
                <div class="card-body">
                    <?php
                        echo $this->Form->create('User');
                        echo $this->Form->input('id',array('type' => 'hidden', 'class' => 'form-control'));
                        echo $this->Form->input('name',array('class' => 'form-control'));
                        echo $this->Form->input('username',array('class' => 'form-control'));
                        echo $this->Form->input('email',array('class' => 'form-control'));
                    ?>
                </div>
                <div class="card-footer"><button type="submit" class="block">Done</button></div>
            </div>
        </div>
    </div>
</div>