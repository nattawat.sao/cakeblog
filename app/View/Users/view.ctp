<!-- File: /app/View/User/view.ctp -->
<body>
<br>
    <div class="container">
        <div class="row ">
            <div class="col-sm-5">
                <div class="card" style="   position: -webkit-sticky; /* Safari */
                                            position: sticky;
                                            top: 2%;">
                    <div class="card-header">
                        <h4 class="text-dark"><b>
                                <center>User Profile</center>
                            </b></h4>
                    </div>
                    <div class="card-body">
                        <img class="card-img-top" src="https://www.w3schools.com/bootstrap4/img_avatar1.png"
                            alt="Card image" style="width:100%">
                    </div>
                    <div class="card-footer">
                        <p>
                            <h5><b>Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</b>&nbsp;&nbsp;<?php echo h($user['User']['name']); ?>
                            </h5>
                        </p>
                        <p>
                            <h5><b>Username&nbsp;&nbsp;&nbsp;:&nbsp;</b>&nbsp;<?php echo $user['User']['username']; ?></h5>
                        </p>
                        <p>
                            <h5><b>Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</b>&nbsp;&nbsp;<?php echo h($user['User']['email']); ?>
                            </h5>
                        </p>
                        
                    </div>
                </div><br>
                <?php if($current_user['id'] == $user['User']['id'] || $current_user['role'] == 'admin'):?>
                        <?php
                                    echo $this->Html->link
                                    (
                                        'Edit',
                                        array('action' => 'edit', $user['User']['id']),
                                        array('style' => 'color: black;','class' => 'block')
                                    );?>
                        <?php endif; ?>
            </div>
            <?php if($current_user['role'] == 'admin'):?>
            <div class='col-sm-7'>
                <div class="card">
                    <div class="card-header">
                        <h4 class="text-dark">
                            <center><b>Post</center></b>
                        </h4>
                    </div>
                    <div class="card-body">
                        <table>
                            <tr>
                                <th>Title</th>
                                <?php if($current_user['role'] == 'admin'):?>
                                <th>Action</th>
                                <?php endif;?>
                                <th>Created</th>
                            </tr>
                            <!-- Here's where we loop through our $posts array, printing out post info -->

                            <?php foreach ($user['Post'] as $post): ?>
                            <tr>
                                <td>
                                    <?php
                                            //var_dump($post);
                                            echo $this->Html->link(
                                                $post['title'],
                                                array('controller' => 'posts', 'action' => 'view', $post['id']),
                                                array('style' => 'color: black;')
                                                );
                                        ?>
                                </td>
                                <td>
                                    <?php
                                    echo $this->Html->link
                                    (
                                        'Edit',
                                        array('controller' => 'posts','action' => 'edit', $post['id']),
                                        array('style' => 'color: black;')
                                    );
                                    ?>&nbsp;&nbsp;

                                    <?php
                                        echo $this->Form->postLink
                                        (
                                            'Delete',
                                            array('controller' => 'posts','action' => 'delete', $post['id']),
                                            array('style' => 'color: black;'),
                                            array('confirm' => 'Are you sure?')
                                        );
                                    ?>&nbsp;&nbsp;
                                </td>
                                <td>
                                    <?php echo $post['created']; ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
                <br>
                <?php if($current_user['id'] == $user['User']['id']):?>
                <a href='http://localhost/cakeblog/Posts/add'><button type="button" class="block">Add post</button></a>
                <!-- <button type="button" id="test" class="block">click me</button><br>
                    <div id="display" style="border:2px solid #00000045; height:300px; width:100%; padding:5px; border-radius: 5px; "></div> -->
                <?php endif;?>
            </div>
        </div>
        <?php endif;?>
        <?php if($current_user['role'] != 'admin'):?>
        <div class='col-sm-7'>
            <?php if($current_user['id'] == $user['User']['id']):?>
            <div class='row'>
                <div class='col-sm-12'>
                    <div class="card">
                        <div class="card-header">
                            <h4 class="text-dark">Write your new content</h4>
                        </div>
                        <div class="card-body">
                            <?php echo $this->Form->create(false, 
                                array('url' => array('controller'=>'posts', 'action'=>'add'),
                                'id' => 'comment','style' => "width: 100%;")); ?>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Title</span>
                                </div>
                                <input name="data[Post][title]" class="form-control" maxlength="50" type="text"
                                    id="PostTitle" required="required" placeholder="Enter your title">
                            </div>
                            <div class="card">
                                <div class="card-header"
                                    style="padding:0.25rem 1.25rem; background-color:#e9ecef; color:#495057; text-align:center;">
                                    Write the content</div>
                                <div class="card-body" style="padding:0.25rem;"><textarea name="data[Post][body]"
                                        class="form-control" rows="3" cols="30" id="PostBody" required="required"
                                        placeholder="write somthing..."></textarea></div>
                            </div>
                        </div>
                        <div class="card-footer"><button type="submit" class="block">Share</button></div>
                    </div>
                    <?php echo $this->Form->end(); ?>
                </div>
            </div><br>
            <?php endif; ?>

            <div class='container'>
                <div class="row">
                    <div class='col-sm-2'>
                    </div>
                    <?php foreach ($post as $post): ?>
                    <div>
                        <div class="card">
                            <div class="card-header">
                                <?php echo $this->Html->link(
                                    $post['title'],
                                    array('controller' => 'posts','action' => 'view', $post['id']),
                                    array('style' => 'color: black;'));
                                ?>
                                <?php if($current_user['id'] == $user['User']['id']):?><br>
                                <?php
                                    echo $this->Html->link
                                    (
                                        'Edit',
                                        array('controller' => 'posts','action' => 'edit', $post['id']),
                                        array('style' => 'color: black;')
                                    );
                                ?>&nbsp;&nbsp;
                                <?php
                                    echo $this->Html->link
                                    (
                                        'Delete',
                                        array('controller' => 'posts','action' => 'delete', $post['id']),
                                        array('style' => 'color: black;'),
                                        array('confirm' => 'Are you sure?')
                                    );
                                ?>
                            <?php endif;?>
                            </div>
                            <div class="card-body">
                                <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo h($post['body']); ?></p>
                                <!-- <?php foreach($post['Image'] as $image): ?>
                                    <?php if($image['name'] != null):?>
                                        <div class = "card">
                                            <div class ="card-boby" style="padding: 5px;">
                                                <img src="<?php echo $this->webroot; ?>img/upload/<?php echo $image['name'] ;?>" width='100%'>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach;?> -->
                            </div>
                            <div class="card-footer">
                                <div class="input-group mb-6">
                                    <input name="comment" class="form-control commentPost"
                                        placeholder="write somthing..." type="text"
                                        id="commentPost-<?php echo $post['id']; ?>" required="required">
                                    <?php echo $this->Form->input('id', array('type' => 'hidden', 'value' => $post['id'])); ?>
                                    <div class="input-group-append">
                                        <button class="btn btn-secondary submit" id="<?php echo $post['id']; ?>">
                                            Comment
                                        </button>
                                    </div>
                                </div>
                                <br>
                                <div id="divComment-<?php echo $post['id']; ?>"></div>
                                <!-- <?php echo $this->Form->create(false, array('url' => array('controller'=>'comments', 'action'=>'add'),'id' => 'comment','style' => "width: 100%;")); ?>comment -->
                                <!-- <?php debug($post)?> -->
                                <?php foreach ($post['comment'] as $comment): ?>
                                <div class='card'>
                                    <div class="card-header">
                                        <?php echo $this->Html->link(
                                    $comment['users']['name'],
                                    array('controller' => 'users','action' => 'view', $comment['users']['id']),
                                    array('style' => 'color: black;'));
                                ?>
                                    </div>
                                    <div class="card-body">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo h($comment['comments']['comment']); ?>
                                    </div>
                                </div>
                                <br>
                                <?php endforeach; ?>
                            </div>
                            <!-- </div><br> <?php echo $this->Form->end(); ?> -->
                        </div><br><?php endforeach; ?>
                    </div>
                    <?php endif;?>
                </div>
            </div>
</body>