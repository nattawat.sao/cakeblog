<br>
<div class='container'>
    <div class='row'>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-8">
            <div class="card">
                <?php echo $this->Form->create('User'); ?>
                    <div class="card-header">
                        <h4 class="text-dark">Add new user</h4>
                    </div>
                    <div class="card-body">
                        
                        <?php
                            echo $this->Form->input('name',array('class' => 'form-control'));
                            echo $this->Form->input('username',array('class' => 'form-control'));
                            echo $this->Form->input('email',array('class' => 'form-control'));
                            echo $this->Form->input('password',array('class' => 'form-control'));
                            echo $this->Form->input('password_confirmation',array('type'=>'password', 'class' => 'form-control'));
                        ?>
                    </div>
                    <div class="card-footer"><button type="submit" class="block">Done</button></div>
                </form>
            </div>
        </div>
    </div>
</div>