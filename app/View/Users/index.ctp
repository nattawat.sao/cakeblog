<!-- File: /app/View/User/index.ctp (edit links added) -->
<br>
<?php if($current_user['role'] == 'admin'):?>
<div class='container'>
    <div class='row'>
        <div class='col-sm-12'>
            <div class="card">
                <div class="card-header">
                    <h4 class="text-dark">Users List</h4>
                </div>
                <div class="card-body">
                    <?php if($current_user['role'] == 'admin'):?>
                    <a href='http://localhost/cakeblog/Users/add'><button class="block" align="center">Create new
                            user</button></a><br>
                    <?php endif;?>
                    <table>
                        <tr>
                            <?php if($current_user['role'] == 'admin'):?>
                            <th>Id</th>
                            <?php endif;?>
                            <th>Name</th>
                            <th>Usename</th>
                            <th>Email</th>
                            <th>Action</th>
                        </tr>
                        <!-- Here's where we loop through our $users array, printing out post info -->
                        <?php foreach ($users as $user):?>
                        <tr>
                            <?php if($current_user['role'] == 'admin'):?>
                            <td><?php echo $user['User']['id']; ?></td>
                            <?php endif;?>
                            <td><?php
                                echo $this->Html->link(
                                $user['User']['name'],
                                array('action' => 'view', $user['User']['id']),
                                array('style' => 'color: black;')
                                );
                                ?></td>
                            <td><?php echo $user['User']['username']; ?></td>
                            <td><?php echo $user['User']['email']; ?></td>
                            <td>
                                <?php if($current_user['id'] == $user['User']['id'] || $current_user['role'] == 'admin'):?>
                                <?php
                                    echo $this->Html->link
                                    (
                                        'Edit',
                                        array('action' => 'edit', $user['User']['id']),
                                        array('style' => 'color: black;')
                                    );
                                    ?>&nbsp;&nbsp;
                                <?php if($current_user['role'] == 'admin'):?>
                                <?php
                                        echo $this->Form->postLink
                                        (
                                            'Delete',
                                            array('action' => 'delete', $user['User']['id']),
                                            array('style' => 'color: black;'),
                                            array('confirm' => 'Are you sure?')
                                        );
                                    ?>&nbsp;&nbsp;
                                <?php
                                        echo $this->Form->postLink
                                        (
                                            'Reset Password',
                                            array('action' => 'confirmToReset', $user['User']['id']),
                                            array('style' => 'color: black;'),
                                            array('confirm' => 'Are you sure?')
                                        );
                                    ?>
                                <?php endif;?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>
<?php if($current_user['role'] != 'admin'):?>
<div class='container'>
        <div class="row">
            <?php foreach ($users as $user):?>
                <?php if(($user['User']['role'] != 'admin') && ($user['User']['id'] != $current_user['id'])):?>
                    <div class ='col-sm-4' style ="padding:15px;">
                        <div class="card">
                            <div class="card-body">
                                <img class="card-img-top" src="https://www.w3schools.com/bootstrap4/img_avatar1.png"
                                    alt="Card image" style="width:100%">
                            </div>
                            <div class="card-footer">
                                    <h5>
                                        <center>
                                            <?php
                                                echo $this->Html->link(
                                                $user['User']['name'],
                                                array('action' => 'view', $user['User']['id']),
                                                array('style' => 'color: black;','text-align: center;')
                                                );
                                            ?>
                                        </center>
                                    </h5>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div><br>
</div>
<?php endif; ?>