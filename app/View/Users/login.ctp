<div class='container'>
    <div class='row'>
        <div class="col-sm-2"></div>
        <div class='col-sm-8'>
            <div class="card" style="margin-top: 15%;">
                <div class="card-header">
                    <h4 class="text-dark">Login</h4>
                </div>
                <?php echo $this->Form->Create();?>
                <div class="card-body">
                    
                    <div class="container">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Username</span>
                            </div>
                            <input name="data[User][username]" class="form-control" maxlength="40" type="text"
                                id="UserUsername" required="required">
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Password &nbsp;</span>
                            </div>
                            <input name="data[User][password]" class="form-control" type="password" id="UserPassword"
                                required="required">
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-9">
                        <button type="submit" class="btn btn-secondary ">Login</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>