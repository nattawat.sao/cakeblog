<br>
<div class='container'>
    <div class='row'>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-8">
            <div class="card">
                <div class="card-header">
                    <h4 class="text-dark">Reset password</h4>
                </div>
                <div class="card-body">
                    <?php
                        echo $this->Form->create();
                    ?>
                    <div class="container">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Password</span>
                            </div>
                            <input name="data[User][password]" type="password" id="UserPassword" required="required">
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">PasswordConfirmation</span>
                            </div>
                            <input name="data[User][password_confirmation]" type="password"
                                id="UserPasswordConfirmation" required="required">
                        </div>
                    </div>
                </div>
                <div class="card-footer"><?php echo $this->Form->end('Save');?></div>
            </div>
        </div>
    </div>
</div>