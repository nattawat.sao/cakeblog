<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = __d('cake_dev', 'Posting');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>

<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php echo $cakeDescription ?>:
        <?php echo $this->fetch('title'); ?>
    </title>
    <?php
		echo $this->Html->meta('icon');

        echo $this->Html->css('cake.generic');
        echo $this->Html->css('custom');
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php WWW_ROOT.'css'.DS.'custom' ;?>">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">



    <!-- fulll width botton -->
    <style>
        .block {
            display: block;
            width: 100%;
            border: none;
            background-color: #CACFD2;
            color: #495057;
            padding: 9px 28px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
            border-radius: 5px;
        }

        A:link {
            text-decoration: none;
        }

        A:visited {
            text-decoration: none;
        }

        .block:hover {
            background-color: #ddd;
            color: black;
        }

        form {
            clear: both;
            margin-right: 20px;
            padding: 0;
            width: 100%;
        }
    </style>
    <!-- polaroid -->
    <style>
        div.polaroid {
        width: 100%;
        background-color: white;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }

        div.container-polaroid-t {
        text-align: center;
        padding: 10px 20px;
        }
        .container .btn-m-edit {
        position: absolute;
        top: 35px;
        left: 75%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        background-color: #f1f1f1;
        color: black;
        font-size: 16px;
        padding: 5px 20px;
        border: none;
        cursor: pointer;
        border-radius: 5px;
        text-align: right;
        }

        .container .btn-m-edit:hover {
        background-color: #888888a1;
        color: #D6D6D6;
        }

        .container .btn-m-delete {
        position: absolute;
        top: 35px;
        left: 90%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        background-color: #f1f1f1;
        color: black;
        font-size: 16px;
        padding: 5px 20px;
        outline : 2 px;
        border: none;
        cursor: pointer;
        border-radius: 5px;
        text-align: right;
        }

        .container .btn-m-delete:hover {
        background-color: #888888a1;
        color: #D6D6D6;
        }
    </style>

    <!-- image resize -->
    <style>

        div.img-resize img {
        width: 100%;
        height: auto;
        }

        div.img-resize {
            width: 100%;
            height: 200px;
            overflow: hidden;
            text-align: center;
        }
    </style>
    <!-- custom image -->
    <style>
        #myImg {
            cursor: pointer;
            transition: 0.3s;
        }

        #myImg:hover {opacity: 0.7;}
    </style>
    <!-- maun menu -->
    <style>
        .main-botton
        {
            display:block;
            height: 40px;
            width: 40px;
            border-radius: 50%;
            background-color:#383838a1;
            border: 0;
            margin: .25rem 1rem .25rem 1rem;
        }

        .main-botton:hover
        {
            background-color: #585858;
            border: 1px;
        }

        .main-botton:visited
        {
            background-color: #212529a1;
        }

        .textAlignVer{
            display:block;
            transform: rotate(90deg);
            font-size:19px;
            color: #f1f1f1;
            width:auto;
            font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";
        }
    </style>
    <!-- setting -->
    <style type="text/css"> 
        #overlay {   
            background:#000;
            width:100%;
            height:100%;
            z-index:80000;
            top:0px;
            left:0px;
            position:fixed;
            opacity: .5;   
            filter: alpha(opacity=50);   
            -moz-opacity: .5;  
            display:none;
        }   
        .msg_show{
            position:fixed;
            z-index:90000;
            margin:auto;
            width:400px;
            height:276px;
            top: 20%;
            left: 50%;
            margin-top: -100px;
            margin-left: -250px;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border: 0px solid #000000;  
            background-color:#ffffffea;
            text-align:center;
            display:none;
        }
    </style> 
</head>
<body style="background-color: #e4e4e4;">
<?php echo $this->fetch('content'); ?>
</body>
</html>
