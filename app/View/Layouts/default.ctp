<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = __d('cake_dev', 'Posting');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>

<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php echo $cakeDescription ?>:
        <?php echo $this->fetch('title'); ?>
    </title>
    <?php
		echo $this->Html->meta('icon');

        echo $this->Html->css('cake.generic');
        echo $this->Html->css('custom');
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php WWW_ROOT.'css'.DS.'custom' ;?>">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Kanit&display=swap" rel="stylesheet">
    <style>
      body {
        font-family: 'Kanit', sans-serif;
      }
    </style>
    <!-- ajax for comment -->
    <script>
        $(function() {
            $(".submit").click(function() {
                var comment = document.getElementById('commentPost-' + $(this).attr('id')).value
                var id = $(this).attr('id')
                console.log(comment)
                $.ajax({
                    url: '/cakeblog/comments/add',
                    cache: false,
                    type: 'POST',
                    data: {
                        "comment": comment,
                        "id": id
                    },
                    success: function(data) {
                        var coment = jQuery.parseJSON(data)
                        $("#divComment-" + id + "").append(
                            "<div class = 'card'>" +
                            "<div class = 'card-header' style = 'padding: 0.25rem 1rem 0.25rem 1rem;'>" +
                            "<div class = 'Userlinkpage'><b>" + coment.User.name +
                            "</b></div>" +
                            "</div>" +
                            "<div class = 'card-body' style = 'padding: 0.25rem 1rem 0.25rem 1rem;'>" +
                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + coment.Comment
                            .comment +
                            "</div>" +
                            "</div>" +
                            "<br>"
                        );
                    }
                });
            });
        });
        $(function() {
            $(".submit").click(function() {
                $(".commentPost").val("");
            });
        });
    </script>
    <!-- main menu -->
    <script>
        function goBack() 
        {
            window.history.back();
        }
        $(document).ready(function()
        {
            $('[data-toggle="tooltip"]').tooltip(); 
        });
        $(function(){
            // Enables popover
            $("[data-toggle=popover]").popover();
        });

        $(document).ready(function()
        {
            $("#btn_m1").click(function()
            {
                $("#btn_m2").slideToggle(200);
                $("#btn_m3").slideToggle(225);
                $("#btn_m4").slideToggle(250);
                $("#btn_m5").slideToggle(256);
                $("#btn_m6").slideToggle(275);
                $("#btn_m7").slideToggle(300);
                $("#btn_m8").slideToggle(325);
                $("#btn_m9").slideToggle(350);
            });
        });

        var prevScrollpos = window.pageYOffset;
        window.onscroll = function() {
        var currentScrollPos = window.pageYOffset;
        if (prevScrollpos > currentScrollPos) {
            document.getElementById("div_menu").style.top = "0";
        } else {
            document.getElementById("div_menu").style.top = "-50px";
        }
        prevScrollpos = currentScrollPos;
        }
    </script>
        <!-- Setting menu -->
    <script>
        $(function(){
            $(".show_box").click(function(){
                $("#overlay").fadeToggle(200,function(){ // แสดงส่วนของ overlay
                    $(".msg_show").slideToggle(200,function(){ // แสดงส่วนของ เนื้อหา popup
                        if($(this).css("display")=="block"){        // ถ้าเป็นกรณีแสดงข้อมูล 
                        //  หากต้องการดึงข้อมูลมาแสดง แบบ ajax
                        //  สามารถดัดแปลงจากโค้ดนี้ได้      
                        //  $(".msg_data").load("data.php");    
                        //      หรือ
                        //  $.post("data.php",{},function(data){
                        //      $(".msg_data").html(data);
                        //  });
                        }
                    });
                });
            });
        });
    </script>
    <!-- Auto hiddent flash message -->
    <script>
        $(document).ready(function(){
            setTimeout(function() {
                $("#flashMessage").fadeOut(600);}, 2000);
        });
    </script>
    <!-- Hidden navbar -->
    <script>
        /* When the user scrolls down, hide the navbar. When the user scrolls up, show the navbar */
        var prevScrollpos = window.pageYOffset;
        window.onscroll = function() {
        var currentScrollPos = window.pageYOffset;
        if (prevScrollpos > currentScrollPos) {
            document.getElementById("navbar").style.top = "0";
        } else {
            document.getElementById("navbar").style.top = "-44px";
        }
        prevScrollpos = currentScrollPos;
        }
    </script>
    <!-- Check display size -->
    <script>
        // $(document).ready(function() {
        //     // This will fire when document is ready:
        //     $(window).resize(function() {
        //         // This will fire each time the window is resized:
        //         if($(window).width() >= 450) {
        //             // if larger or equal
        //             $('#navbar').show();
        //         } else {
        //             // if smaller
        //             $('#navbar').hid();
        //         }
        //     }).resize(); // This will simulate a resize to trigger the initial run.
        // });
    </script>
    <!-- fulll width botton -->
    <style>
        .block {
            display: block;
            width: 100%;
            border: none;
            background-color: #cacfd2;
            color: #495057;
            padding: 9px 28px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
            border-radius: 5px;
        }

        A:link {
            text-decoration: none;
        }

        A:visited {
            text-decoration: none;
        }

        .block:hover {
            background-color: #A9A9A9;
            color: #495057;
        }

        form {
            clear: both;
            margin-right: 20px;
            padding: 0;
            width: 100%;
        }
    </style>
    <!-- polaroid -->
    <style>
        div.polaroid {
        width: 100%;
        background-color: white;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }

        div.container-polaroid-t {
        text-align: center;
        padding: 10px 20px;
        }
        .container .btn-m-edit {
        position: absolute;
        top: 35px;
        left: 75%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        background-color: #f1f1f1;
        color: black;
        font-size: 16px;
        padding: 5px 20px;
        border: none;
        cursor: pointer;
        border-radius: 5px;
        text-align: right;
        }

        .container .btn-m-edit:hover {
        background-color: #888888a1;
        color: #D6D6D6;
        }

        .container .btn-m-delete {
        position: absolute;
        top: 35px;
        left: 90%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        background-color: #f1f1f1;
        color: black;
        font-size: 16px;
        padding: 5px 20px;
        outline : 2 px;
        border: none;
        cursor: pointer;
        border-radius: 5px;
        text-align: right;
        }

        .container .btn-m-delete:hover {
        background-color: #888888a1;
        color: #D6D6D6;
        }
    </style>

    <!-- image resize -->
    <style>

        div.img-resize img {
        width: 100%;
        height: auto;
        }

        div.img-resize {
            width: 100%;
            height: 200px;
            overflow: hidden;
            text-align: center;
        }
    </style>
    <!-- custom image -->
    <style>
        #myImg {
            cursor: pointer;
            transition: 0.3s;
        }

        #myImg:hover {opacity: 0.7;}
    </style>
    <!-- maun menu -->
    <style>
        .main-botton
        {
            display:block;
            height: 40px;
            width: 40px;
            border-radius: 50%;
            background-color:#383838a1;
            border: 0;
            margin: .25rem 1rem .25rem 1rem;
            top: 3%;
        }

        .main-botton:hover
        {
            background-color: #585858;
            border: 1px;
        }

        .main-botton:visited
        {
            background-color: #212529a1;
        }

        .textAlignVer{
            display:block;
            transform: rotate(90deg);
            font-size:19px;
            color: #f1f1f1;
            width:auto;
        }
    </style>
    <!-- setting -->
    <style type="text/css"> 
        #overlay {   
            background:#000;
            width:100%;
            height:100%;
            z-index:80000;
            top:0px;
            left:0px;
            position:fixed;
            opacity: .5;   
            filter: alpha(opacity=50);   
            -moz-opacity: .5;  
            display:none;
        }   
        .msg_show{
            position:fixed;
            z-index:90000;
            margin:auto;
            width:400px;
            height:276px;
            top: 20%;
            left: 50%;
            margin-top: 0px;
            margin-left: -12.5rem;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border: 0px solid #000000;  
            background-color:#ffffffea;
            text-align:center;
            display:none;
        }
    </style> 
    <!-- Hidden navbar -->
    <style>
        #navbar {
            background-color: #000000a1; /* Black background color */
            position: fixed; /* Make it stick/fixed */
            top: 0; /* Stay on top */
            width: 100%; /* Full width */
            transition: top 0.3s; /* Transition effect when sliding down (and up) */
            }

            /* Style the navbar links */
        #navbar a {
            float: left;
            display: block;
            color: white;
            text-align: center;
            padding: 10px;
            text-decoration: none;
            }

        #navbar a:hover {
            background-color: #A9A9A9a1;
            color: #000;
            }
    </style>
    <!-- Navbar right position -->
    <style>
        /* Right-aligned section inside the top navigation */
        .topnav-right {
        float: right;
        }
    </style>
    
</head>
<body style="background-color: #e4e4e4;">
    <div id="container">
    <?php 
    $Condition_page1 = "/cakeblog/";
    $Condition_page2 = "/cakeblog/users/register";
    $Condition_page3 = "/cakeblog/users/Login";
    $Condition_page4 = "/cakeblog/users/resetpassword/".$current_user['id'];
    $Condition_page5 = "/cakeblog/posts";
    $Condition_page6 = "/cakeblog/users";
    if( isset($post['Post']['id']) && ($post['Post']['id']!==null) )
    {
        $Condition_page7 = "/cakeblog/posts/view/".$post['Post']['id'];
    }
    $currentpage = $_SERVER['REQUEST_URI'];
    if( ($Condition_page1!=$currentpage) && 
        ($Condition_page2!=$currentpage) && 
        ($Condition_page3!=$currentpage) && 
        ($Condition_page4!=$currentpage)):?>
        <?php if($current_user['menu_style'] == 'modern'):?>
            <div class="fixed-top col-1"><br>
                <button class = "btn"
                        id = "btn_m1"
                        style="
                            display:block;
                            height: 40px;
                            width: 40px;
                            border-radius: 50%;
                            background-color:#000000c1;
                            border: 0;
                            margin:0rem 0rem 0rem 1rem;
                            padding:0px"
                        data-toggle="tooltip" data-placement="right" title="Menu">
                        <i style='font-size:17px; color:#fff;' class='fas'>&#xf0c9;</i>
                </button>
                <div id = "div_menu">
                    <button class = "btn main-botton"
                            id = "btn_m2"
                            onclick="goBack()"
                            data-toggle="tooltip" data-placement="right" title="Back">
                            <i style='font-size:24px; color:#fff;' class='fas'>&#xf104;</i>
                    </button>
                    <?php if($current_user['role'] != 'admin'):?>
                        <button class = "btn main-botton"
                                id = "btn_m3"
                                onclick="window.location.href='/cakeblog/posts'"
                                data-toggle="tooltip" data-placement="right" title="Home">
                                <i style='font-size:17px; color:#fff;' class='fas'>&#xf015;</i>
                        </button>
                        <button class = "btn main-botton"
                                id = "btn_m4"
                                onclick="window.location.href='<?php echo $this->Html->url(array('controller' => 'users','action' => 'view', $current_user['id']));?>'"
                                data-toggle="tooltip" data-placement="right" title="<?php echo $current_user['name']?>">
                                <i style='font-size:20px; color:#fff;' class='fas'>&#xf007;</i>
                        </button>
                        <button class = "btn main-botton"
                                id = "btn_m5"
                                onclick="window.location.href='/cakeblog/users'"
                                data-toggle="tooltip" data-placement="right" title="suggest friend">
                                <i style='font-size:17px; color:#fff;' class='fas'>&#xf234;</i>
                        </button>
                    <?php endif;?>
                    <?php if($current_user['role'] == 'admin'):?>
                        <button class = "btn main-botton"
                                id = "btn_m3"
                                onclick="window.location.href='<?php echo $this->Html->url(array('controller' => 'users','action' => 'view', $current_user['id']));?>'"
                                data-toggle="tooltip" data-placement="right" title="Admin">
                                <i style='font-size:20px; color:#fff;' class='fas'>&#xf007;</i>
                        </button>
                        <button class = "btn main-botton"
                                id = "btn_m4"
                                onclick="window.location.href='/cakeblog/posts'"
                                data-toggle="tooltip" data-placement="right" title="Post List">
                                <i style='font-size:17px; color:#fff;' class='fas'>&#xf022;</i>
                        </button>
                        <button class = "btn main-botton"
                                id = "btn_m5"
                                onclick="window.location.href='/cakeblog/users'"
                                data-toggle="tooltip" data-placement="right" title="User List">
                                <i style='font-size:17px; color:#fff;' class='fas'>&#xf2b9;</i>
                        </button>
                    <?php endif;?>
                    <button class = "btn main-botton show_box"
                            id = "btn_m6"
                            href='javascript:void(0);'
                            data-toggle="tooltip" data-placement="right" title="Setting">
                            <i style='font-size:20px; color:#fff;' class='fas'>&#xf0ad;</i>
                    </button>
                    <button class = " btn main-botton"
                            id = "btn_m7"
                            onclick="window.location.href='<?php echo $this->Html->url(array('controller'=>'users', 'action' => 'logout'));?>'"
                            data-toggle="tooltip" data-placement="right" title="Log out">
                            <i style='font-size:18px; color:#fff; text-align:center;' class='fa'>&#xf08b;</i>
                    </button>
                    <div style="display:block;
                                height: 250px;
                                width: 40px;
                                border-radius: 40px ;
                                background-color:#383838a1;
                                border: 0;
                                margin: .25rem 1rem .25rem 1rem;"
                                id = "btn_m8">
                            <img src="https://www.w3schools.com/bootstrap4/img_avatar1.png" alt="John Doe" class="rounded-circle" style="width:36px;margin:2px">
                            <p class = "textAlignVer">&nbsp;&nbsp;&nbsp;<?php echo $current_user['username']; ?></p>
                    </div>
                    <?php if(($current_user['role'] == 'admin') && ($Condition_page5 == $currentpage)):?>
                        <button class = " btn main-botton"
                                id = "btn_m9"
                                onclick="window.location.href='<?php echo $this->Html->url(array('controller'=>'posts', 'action' => 'exportReport_Post'));?>'"
                                data-toggle="tooltip" data-placement="right" title="Export to CSV">
                                <i style='font-size:18px; color:#fff; text-align:center;' class='fa'>&#xf1c3;</i>
                        </button>
                    <?php endif;?>
                    <?php if(($current_user['role'] == 'admin') && ($Condition_page6 == $currentpage)):?>
                        <button class = " btn main-botton"
                                id = "btn_m9"
                                onclick="window.location.href='<?php echo $this->Html->url(array('controller'=>'users', 'action' => 'exportReport_User'));?>'"
                                data-toggle="tooltip" data-placement="right" title="Export to CSV">
                                <i style='font-size:18px; color:#fff; text-align:center;' class='fa'>&#xf1c3;</i>
                        </button>
                    <?php endif;?>
                    <?php if(((isset($post['Post']['id'])) && ($post['Post']['id']!==null)) && 
                                ($current_user['role'] == 'admin') && ($Condition_page7 == $currentpage)):?>
                        <button class = " btn main-botton"
                                id = "btn_m9"
                                onclick="window.location.href='<?php echo $this->Html->url(array('controller'=>'posts', 'action' => 'PDFview',$post['Post']['id']));?>'"
                                data-toggle="tooltip" data-placement="right" title="Export to PDF">
                                <i style='font-size:18px; color:#fff; text-align:center;' class='fa'>&#xf1c1;</i>
                        </button>
                    <?php endif;?>
                </div>
            </div>
        <?php endif; ?>
        <?php if($current_user['menu_style'] == 'default'):?>
            <div class="topnav fixed-top" id="navbar">
                <?php if($current_user['role'] == 'admin'):?>
                    <a href="/cakeblog/users" style="color: rgb(255, 255, 255);">User List</a>
                    <a href="/cakeblog/posts" style="color: rgb(255, 255, 255);">Post List</a>
                    <?php if($Condition_page5 == $currentpage):?>
                            <a href="<?php echo $this->Html->url(array('controller'=>'posts', 'action' => 'exportReport_Post'));?>" style="color: rgb(255, 255, 255);">Export</a>
                    <?php endif; ?>
                    <?php if($Condition_page6 == $currentpage):?>
                            <a href="<?php echo $this->Html->url(array('controller'=>'users', 'action' => 'exportReport_User'));?>" style="color: rgb(255, 255, 255);">Export</a>    
                    <?php endif; ?>
                    <?php if(((isset($post['Post']['id'])) && ($post['Post']['id']!==null)) && $Condition_page7 == $currentpage):?>
                            <a href="<?php echo $this->Html->url(array('controller'=>'posts', 'action' => 'PDFview',$post['Post']['id']));?>" style="color: rgb(255, 255, 255);">Export</a>                             
                    <?php endif;?>
                <?php endif; ?>
                <?php if($current_user['role'] != 'admin'):?>
                    <a class="nav-link" href="/cakeblog/posts" style="color: rgb(255, 255, 255);">Home</a>                       
                    <a class="nav-link" href="/cakeblog/users" style="color: rgb(255, 255, 255);">Friends</a>
                <?php endif; ?>
                <div class="topnav-right">
                    <?php 
                        echo $this->Html->link($current_user['name'],
                        array('controller' => 'users','action' => 'view', $current_user['id']),
                        array('style' => 'color: rgb(255, 255, 255);'));
                    ?>
                    <a  class = "show_box" 
                        href='javascript:void(0);' 
                        style = "color: rgb(255, 255, 255);">
                        Setting
                    </a>
                    <?php 
                        echo $this->Html->link('Logout',
                        array('controller'=>'users', 'action' => 'logout'),
                        array('style' => 'color: rgb(255, 255, 255); padding: 10px 20px 10px 10px;'));?></b>
                </div>
            </div><br><br>
        <?php endif; ?>
    <?php endif; ?>
    <?php if(!$logged_in): ?>
    <div class="fixed-top col-1"><br>
        <button class = "btn main-botton"
                onclick="window.location.href='<?php echo $this->Html->url(array('controller'=>'users','action' => 'Login'));?>'"
                data-toggle="tooltip" data-placement="right" title="Log in">
                <i style='font-size:20px; color:#fff; ' class='fa'>&#xf007;</i>
        </button>
        <button class = "btn main-botton"
                onclick="window.location.href='<?php echo $this->Html->url(array('controller'=>'users','action' => 'register'));?>'"
                data-toggle="tooltip" data-placement="right" title="Sign in">
                <i style='font-size:20px; color:#fff;' class='fa'>&#xf234;</i>
        </button>
        
    </div>
    <?php endif; ?>
    <?php if($Condition_page4 == $currentpage): ?>
        <div class="fixed-top col-1"><br>
            <button class = " btn main-botton"
                    id = "btn_m5"
                    onclick="window.location.href='<?php echo $this->Html->url(array('controller'=>'users', 'action' => 'logout'));?>'"
                    data-toggle="tooltip" data-placement="right" title="Log out">
                    <i style='font-size:18px; color:#fff; text-align:center;' class='fa'>&#xf08b;</i>
            </button>
        </div>
    <?php endif; ?>
        <?php echo $this->Session->flash('auth')?>
        <?php echo $this->Flash->render(); ?>
        <?php echo $this->fetch('content'); ?>
    </div>
    <div class="msg_show" align = "center">
        <div class="card msg_data">
            <div class = "card-header" style ="padding:5px;">
                <h6>Setting</h6>
            </div>
            <?php echo $this->Form->create(false, 
                        array('type' => 'file','url' => 
                            array('controller'=>'users', 'action'=>'setting', $current_user['id']))); ?>
            
            <div class = "card-body" style="forn-sixe:18px; text-align:left;">
                <div class = "row">
                    <div class = "card col" style = "padding:0px;">
                        <div class = "card-header" style="padding: .25rem .25rem;">
                            <p style="margin:0rem; text-align:center;">main menu</p>
                        </div>
                        <div class = "card-body" style="padding: .25rem;">
                            <div class = "row">
                                <div class="col">
                                    <div class = "btn main-botton" style="border-radius: 0px ;"><p style='font-size:18px; color:#fff;'><b>H</b></p></div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="Menu_style1" name="data[User][menu_style]" value="default" required <?php if($current_user['menu_style'] == 'default'):?>checked<?php endif; ?>>
                                        <label class="custom-control-label" for="Menu_style1">Default</label>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class = "btn main-botton"><i style='font-size:18px; color:#fff;' class='fas'>&#xf0c9;</i></div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="Menu_style2" name="data[User][menu_style]" value="modern" required <?php if($current_user['menu_style'] == 'modern'):?>checked<?php endif; ?>>
                                        <label class="custom-control-label" for="Menu_style2">Modern</label>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer" style ="padding:5px;">
            <button
                type="submit" 
                class="block " 
                style="padding: 2px 28px;">Done</button>
            </div>
            </form>
            <div class="card-footer" style ="padding:5px;">
                <button
                onclick="window.location.href='javascript:void(0);'"
                class="block show_box"
                style="padding: 2px 28px;">Close</button>
            </div> 
        </div>
    </div>
    <div id="overlay" ></div> 
</body>
</html>
