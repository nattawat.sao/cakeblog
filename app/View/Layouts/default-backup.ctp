<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = __d('cake_dev', 'Posting');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>

<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php echo $cakeDescription ?>:
        <?php echo $this->fetch('title'); ?>
    </title>
    <?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('cake.generic');
		echo $this->Html->css('custom');
		// echo $this->Html->script('comment');
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- ajax for comment -->
    <script>
        $(function() {
            $(".submit").click(function() {
                var comment = document.getElementById('commentPost-' + $(this).attr('id')).value
                var id = $(this).attr('id')
                $.ajax({
                    url: '/cakeblog/comments/add',
                    cache: false,
                    type: 'POST',
                    data: {
                        "comment": comment,
                        "id": id
                    },
                    success: function(data) {
                        var coment = jQuery.parseJSON(data)
                        $("#divComment-" + id + "").append(
                            "<div class = 'card'>" +
                            "<div class = 'card-header' style = 'padding: 0.25rem 1rem 0.25rem 1rem;'>" +
                            "<div class = 'Userlinkpage'><b>" + coment.User.name +
                            "</b></div>" +
                            "</div>" +
                            "<div class = 'card-body' style = 'padding: 0.25rem 1rem 0.25rem 1rem;'>" +
                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + coment.Comment
                            .comment +
                            "</div>" +
                            "</div>" +
                            "<br>"
                        );
                    }
                });
            });
        });
        $(function() {
            $(".submit").click(function() {
                $(".commentPost").val("");
            });
        });
    </script>
    <!-- fulll width botton -->
    <style>
        .block {
            display: block;
            width: 100%;
            border: none;
            background-color: #CACFD2;
            color: #495057;
            padding: 9px 28px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
            border-radius: 5px;
        }

        A:link {
            text-decoration: none;
        }

        A:visited {
            text-decoration: none;
        }

        .block:hover {
            background-color: #ddd;
            color: black;
        }

        form {
            clear: both;
            margin-right: 20px;
            padding: 0;
            width: 100%;
        }
    </style>
    <!-- polaroid -->
    <style>
        div.polaroid {
        width: 100%;
        background-color: white;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }

        div.container-polaroid-t {
        text-align: center;
        padding: 10px 20px;
        }
        .container .btn-m-edit {
        position: absolute;
        top: 35px;
        left: 75%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        background-color: #f1f1f1;
        color: black;
        font-size: 16px;
        padding: 5px 20px;
        border: none;
        cursor: pointer;
        border-radius: 5px;
        text-align: right;
        }

        .container .btn-m-edit:hover {
        background-color: #545454;
        color: #D6D6D6;
        }

        .container .btn-m-delete {
        position: absolute;
        top: 35px;
        left: 90%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        background-color: #f1f1f1;
        color: black;
        font-size: 16px;
        padding: 5px 20px;
        outline : 2 px;
        border: none;
        cursor: pointer;
        border-radius: 5px;
        text-align: right;
        }

        .container .btn-m-delete:hover {
        background-color: #545454;
        color: #D6D6D6;
        }
    </style>

    <!-- image resize -->
    <style>

        div.img-resize img {
        width: 100%;
        height: auto;
        }

        div.img-resize {
            width: 100%;
            height: 200px;
            overflow: hidden;
            text-align: center;
        }
    </style>
    <!-- custom image -->
    <style>
        #myImg {
            cursor: pointer;
            transition: 0.3s;
        }

        #myImg:hover {opacity: 0.7;}
    </style>
</head>
<body style="background-color: #e4e4e4 !important;">
    <div id="container">
    <?php 
    $homepage = "/cakeblog/";
    $currentpage = $_SERVER['REQUEST_URI'];
    if($homepage!=$currentpage):?>
        <div class="row fixed-top" >
            <nav class="navbar navbar-expand-sm bg-light navbar-light col-sm-9"
                style="background-color: #000000a1 !important; padding:0rem">
                <ul class="navbar-nav">
                    <?php if($current_user['role'] == 'admin'):?>
                    <li class="nav-item">
                        <a class="nav-link" href="/cakeblog/users" style="color: rgb(255, 255, 255);">User List</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/cakeblog/posts" style="color: rgb(255, 255, 255);">Post List</a>
                    </li>
                    <?php endif; ?>
                    <?php if($current_user['role'] != 'admin'):?>
                    <li class="nav-item">
                        <a class="nav-link" href="/cakeblog/posts" style="color: rgb(255, 255, 255);">Home</a>
                    </li>
                </ul>
                <?php endif; ?>
            </nav>
            <nav class="navbar navbar-expand-sm bg-light navbar-light col-sm-3"
                style="background-color: #000000a1 !important; padding:0rem">
                <ul class="nav justify-content-end">
                    <li class="nav-item">
                        <?php if($logged_in): ?>
                        <?php 
                            echo $this->Html->link($current_user['name'],
                            array('controller' => 'users','action' => 'view', $current_user['id']),
                            array('style' => 'color: rgb(255, 255, 255);'));
                        ?>
                        <?php 
                            echo $this->Html->link('Logout',
                            array('controller'=>'users', 'action' => 'logout'),
                            array('style' => 'color: rgb(255, 255, 255);'));?></b>
                        <?php else : ?>
                        <?php
                            echo $this->Html->link('Login',
                            array('controller'=>'users', 'action' => 'Login'),
                            array('style' => 'color: rgb(255, 255, 255);'));?>
                        <?php endif; ?>
                    </li>
                </ul>
            </nav>
        </div>
        <br><br>
        <?php endif; ?>
        <?php echo $this->Session->flash('auth')?>
        <?php echo $this->Flash->render(); ?>
        <?php echo $this->fetch('content'); ?>
    </div>
</body>
</html>