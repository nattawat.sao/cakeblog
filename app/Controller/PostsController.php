<?php
class PostsController extends AppController 
{
    public $user = array('Images', 'Posts');
    public $helpers = array('Html', 'Form');
    public $components = array(
        'RequestHandler',
        "Flash",
        'Export.Export'
        // 'Upload'
    );
    public function index() 
    {
        $current_user = $this->Auth->user();
        if($current_user['role'] == 'admin')
        {
            $this->set('posts',$this->Post->find('all',array('order' => 'Post.id')));
        }
        if($current_user['role'] != 'admin')
        {
            $displayPost = $this->Post->find('all',
                    array(
                        'order'=>array('Post.modified' => 'desc')
                    )
                );
            $this->set('posts',$displayPost);
        }       
    }
    public function view($id = null) 
    {
        // $Image = ClassRegistry::init('Image');
        if (!$id) 
        {
            throw new NotFoundException(__('Invalid post'));
        }
        $post = $this->Post->findById($id);
        if (!$post) 
        {
            throw new NotFoundException(__('Invalid post'));
        }
        $this->pdfConfig = array(
            'filename' => 'Report.pdf',
            'download' => true
        );
        $this->set('post', $post);
    }
    
    public function add() 
    {Configure::write('debug', 3);
        if ($this->request->is('post')) {
            // debug($this->request->data); exit;
            $this->Post->create();
            $this->request->data['Post']['user_id'] = $this->Auth->user('id');
            $post = $this->Post->save($this->request->data);
            if ($post) {
                foreach ($this->request->data['Post']['image_name'] as $name => $imageName)
                {
                    // $frmData = $this->request->data;
                    $uploadFolder = WWW_ROOT.'img'.DS.'upload'.DS.$post['Post']['id'];
                    $filename = time().'_'.$imageName['name'];
                    $uploaPath = $uploadFolder . DS . $filename;
                    if (!file_exists($uploadFolder)) {
                        mkdir($uploadFolder);
                    }
                    if (move_uploaded_file($imageName['tmp_name'], $uploaPath)) 
                    {
                        $Image = ClassRegistry::init('Image');
                        $Image->Create();
                        $images = $Image->save(array(
                            'name' => $filename,
                            'path' => $uploaPath,
                            'post_id' => $post['Post']['id'],
                            'created' => $post['Post']['created'],
                            'modified' => $post['Post']['modified'],
                            'user_id' => $post['Post']['user_id']
                        ));
                    }
                }
                $this->Flash->success(__('The post has been saved'));
                return $this->redirect(array('action' => 'index'));
            }
            else
            {
                $this->Session->error(__('Unable to add your post.'));
            }
        }
    }
    public function edit($id = null) 
    {
        if (!$id) 
        {
            throw new NotFoundException(__('Invalid post'));
        }
        $post = $this->Post->findById($id);
        if (!$post) 
        {
            throw new NotFoundException(__('Invalid post'));
        }
        
        if ($this->request->is(array('post', 'put'))) 
        {   
            // debug($this->request->data); exit;
            $this->Post->id = $id;
            $this->request->data['Post']['user_id'] = $this->Auth->user('id');
            $post = $this->Post->save($this->request->data);
            if($post) 
            {
                foreach ($this->request->data['Post']['image_name'] as $name => $imageName)
                {
                    $uploadFolder = WWW_ROOT.'img'.DS.'upload'.DS.$post['Post']['id'];
                    $filename = time().'_'.$imageName['name'];
                    $uploaPath = $uploadFolder . DS . $filename;
                    if (!file_exists($uploadFolder)) {
                        mkdir($uploadFolder);
                    }
                    if (move_uploaded_file($imageName['tmp_name'], $uploaPath)) 
                    {
                        $Image = ClassRegistry::init('Image');
                        $Image->Create();
                        $images = $Image->save(array(
                            'name' => $filename,
                            'path' => $uploaPath,
                            'post_id' => $post['Post']['id'],
                            // 'created' => $post['Post']['created'],
                            'modified' => $post['Post']['modified'],
                            'user_id' => $post['Post']['user_id']
                        ));
                    }
                }
                // var_dump($this->Flash);
                $this->Flash->success(__('Your post has been updated.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Unable to update your post.'));
        }
        
        if (!$this->request->data) 
        {
            $this->request->data = $post;
        }
    }
    public function delete($id = null)
    {
        App::uses('Folder', 'Utility');
        $Image = ClassRegistry::init('Image');
        // $uploadFolder = WWW_ROOT.'img'.DS.'upload'.DS.$id;
        $uploadFolder = new Folder(WWW_ROOT.'img'.DS.'upload'.DS.$id);
        // debug($uploadFolder); exit;
        if ($this->Post->Comment->deleteAll(array('Comment.post_id'=> $id)) &&  
            $Image->deleteAll(array('Image.post_id'=> $id)) &&
            $this->Post->delete(array('Post.id'=> $id)))
        {
            if($uploadFolder->delete()) 
            {
            }
            $this->Flash->error(__('The post has been deleted.'));
        } 
        else 
        {
            $this->Flash->error(__('The post could not be deleted.'));
        }
        return $this->redirect(array('controller' => 'posts','action' => 'index'));
    }
    
    public function deleteImg ($id = null)
    {
        App::uses('Folder', 'Utility');
        $Image = ClassRegistry::init('Image');
        $imageSelect = $Image->findById($id);
        $post_id = $imageSelect['Image']['post_id'];
        $imageName = $imageSelect['Image']['name'];
        $DeleteParth = new File(WWW_ROOT.'img'.DS.'upload'.DS.$post_id.DS.$imageName);
        if ($Image->deleteAll(array('Image.id'=> $id)))
        {
            if($DeleteParth->delete())
            {
                $this->Flash->error(__('The post has been deleted.'));
                return $this->redirect(array('controller' => 'posts','action' => 'edit',$post_id));
            }
        } 
        else 
        {
            $this->Flash->error(__('The post could not be deleted.'));
        }
    }   
    public function exportReport_Post() {
        $data = $this->Post->find('all');
        foreach($data as $PostData)
        {
            $Post[] = $PostData['Post'];
        }
        $this->Export->exportCsv($Post, 'PostReport.csv');
        // return $this->redirect(array('controller' => 'posts','action' => 'index'));
    }

    public function PDFview($id = null) {
        $data = $this->Post->findById($id);
        $PostData = $data['Post'];
        // debug($PostData);exit;
        if (!$this->Post->exists()) {
            throw new NotFoundException(__('Invalid invoice'));
        }
        $this->pdfConfig = array(
            'filename' => 'Report.pdf',
            'download' => true
        );
        $this->set('Post', $this->Post->read(null, $id));
        return $this->render('\PDFview');
    }
}
?>