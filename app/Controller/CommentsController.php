<?php
class CommentsController extends AppController 
{
    public $helpers = array('Html', 'Form');
    public $components = array(
        'RequestHandler'
    );
    public function add()
    {
        $this->autoRender = false; // avoid to render view
        $current_user = $this->Auth->user();
        // debug($this->request->data);
            $this->Comment->create();
            $this->request->data['user_id'] = $this->Auth->user('id');
            $this->request->data['post_id'] = $this->request->data['id'];
            unset($this->request->data['id']);
            // exit;
            if ($this->Comment->save($this->request->data))
            {     
                $comment = $this->postUser();
                return json_encode($comment);
            }
    }
    public function postUser() 
    {
        $current_user = $this->Auth->user();
        // $Post = ClassRegistry::init('Post');
        if($current_user['role'] == 'admin')
        {
            return $this->Comment->find('first',array('order' => 'Comment.date'));
        }
        if($current_user['role'] != 'admin')
        {
            $displayPost = $this->Comment->find('first',
                    array(
                        'order'=>array('Comment.date' => 'desc')
                    )
                );
                return $displayPost;
        }       
    }
}
?>