<?php
class UsersController extends AppController 
{
    public $components = array(
        'RequestHandler',
        "Flash",
        'Export.Export'
        // 'Upload'
    );
    public $user = array('Users','Posts');
    // public $post = array('Posts');
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow('add');
    }
    public function isAuthorized($user)
    {
        if($user['role']== 'admin')
        {
            return true;
        }
        if(in_array($this->action,array('edit','delete')))
        {
            if($user['id'] != $this->request->params['pass'][0])
            {
                return false;
            }
        }
        return true;
    }
    public function login()
    {
        if($this->request->is('post'))
        {
           if($this->Auth->login())
           {
               if($this->Auth->user('resetpassword') == '1')
               {
                $this->redirect(array('action' => 'resetpassword',$this->Auth->user('id')));
               }

                $this->redirect(array('action' => 'view',$this->Auth->user('id')));
           }
           else
           {
                $this->Flash->error('Your username of password combination was incorrect');
           }
        }
    }
    public function logout()
    {
        $this->redirect($this->Auth->logout());
    }
    public function view($id = null) 
    {
        if (!$id) 
        {
            throw new NotFoundException(__('Invalid user'));
        }
        $user = $this->User->findById($id);
        $post = $this->User->searchComment($id);
        if (!$user) 
        {
            throw new NotFoundException(__('Invalid user'));
        }

        $this->set(compact('post','user'));
    }
    public function index() 
    {
        $this->Auth->user();
        $reset = ($this->Auth->user('resetpassword'));

        if($reset == 1)
        {
            $id = ($this->Auth->user('id'));
            return $this->redirect(array('action' => 'resetpassword',$id));
        }
            $users  = array();
            // if($this->Auth->user('role') != 'admin' && $this->Auth->user() != null){
            //     $user = $this->User->findById($this->Auth->user('id'));
            //     $users[0] = $user; 
            // }else
            // {
                $users =  $this->User->find('all');
            // }
            $this->set('users', $users);
    }
    public function register()
    {
        if ($this->request->is('post')) 
        {
            $this->User->create();
            if ($this->User->save($this->request->data)) 
            {
                $this->Flash->success('The user has been saved');
                return $this->redirect(array('action' => 'login'));
            }
            else
            {
                $this->Flash->error('The user could not be saved. Please try again.');
            }
        }
    }
    public function add()
    {
        if ($this->request->is('post')) 
        {
            $this->User->create();
            if ($this->User->save($this->request->data))
            {
                $this->Flash->success('The user has been saved');
                return $this->redirect(array('controller' => 'users','action' => 'index'));
            }
            else
            {
                $this->Flash->error('The user could not be saved. Please try again.');
            }
        }
    }
    public function edit($id = null)
    {
        if (!$id) 
        {
            throw new NotFoundException(__('Invalid user'));
        }
        $user = $this->User->findById($id);
        if (!$user) 
        {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array('post', 'put'))) 
        {
            $this->User->id = $id;
            if ($this->User->save($this->request->data)) 
            {
                $this->Session->write('Auth', $this->User->read(null, $this->Auth->User('id')));
                $this->Flash->success('Your user has been updated.');
                return $this->redirect(array('controller' => 'users','action' => 'view',$id));
            }
            $this->Flash->error('Unable to update your user.');
        }
        if (!$this->request->data) 
        {
            $this->request->data = $user;
        }
    }
    public function confirmToReset ($id = null)
    {
        if (!$id)
        {
            throw new NotFoundException(__('Invalid user'));
        }
        $user = $this->User->findById($id);
        $this->User->id = $id;
        $this->User->save(array('User' => array('resetpassword' => '1')));
        return $this->redirect(array('action' => 'index'));
    }
    public function resetpassword($id = null)
    {
        if (!$id) 
        {
            throw new NotFoundException(__('Invalid user'));
        }
        $user = $this->User->findById($id);
        if (!$user) 
        {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array('post'))) 
        {
            $this->User->id = $id;
            if ($this->User->save($this->request->data)) 
            {
                $this->Flash->success('Your user has been updated.');
                $this->User->save(array('User' => array('resetpassword' => '0')));
                return $this->logout();
            }
        }
    }
    public function delete($id) 
    {
        if ($this->request->is('get')) 
        {
            throw new MethodNotAllowedException();
        }
        if ($this->User->Post->deleteAll(array('Post.user_id'=> $id)) && $this->User->delete($id)) 
        {
            $this->Flash->error('The user has been deleted.');
        }
        else 
        {
            $this->Flash->error('The user could not be deleted.');
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function setting($id = null)
    {
        if (!$id) 
        {
            throw new NotFoundException(__('Invalid user'));
        }
        $user = $this->User->findById($id);
        if (!$user) 
        {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array('post'))) 
        {
            $oldStyle = $this->Auth->User('menu_style');
            $newStyle = $this->request->data['User']['menu_style'];
            $this->User->id = $id;
            if($oldStyle != $newStyle)
            {
                if ($this->User->save($this->request->data)) 
                {
                    $this->Session->write('Auth', $this->User->read(null, $this->Auth->User('id')));
                    $this->Flash->success('Changed your style.');
                    return $this->redirect(array('controller' => 'posts','action' => 'index'));
                }
            }
            else
            {
                $this->Session->setFlash('This style is used.');
                return $this->redirect(array('controller' => 'posts','action' => 'index'));
            }
        }
    }
    public function exportReport_User() {
        $data = $this->User->find('all');
        foreach($data as $UserData)
        {
            $User[] = $UserData['User'];
        }
        $this->Export->exportCsv($User, 'UserReport.csv');
    }
}
?>