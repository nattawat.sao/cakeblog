<?php
    class Comment extends AppModel 
    {
        public $validate = array
        (
            'user_id' => array('rule' => 'notBlank'),
            'post_id' => array('rule' => 'notBlank'),
            'comment' => array('rule' => 'notBlank')
        );

        public $belongsTo = array(
            'User' => array(
                'className' => 'User',
                'foreignKey' => 'user_id'
            ),
            'Post' => array(
                'className' => 'Post',
                'foreignKey' => 'post_id'
            )
        );
    }
?>