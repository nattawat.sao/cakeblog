<?php
    class User extends AppModel
    {
        public $validate = array
        (
            'name'=> array('Please enter your name.'=>
                        array ( 'rule' => 'notBlank',
                                'message' => 'Please enter your name.'
                )
            ),
            
            'username'=>array('Please enter your username.'=>
                        array ( 'rule' => 'notBlank',
                                'message' => 'Please enter your username.'
                )
            ),

            'email'=>array('Please enter your email.'=>
                        array ( 'rule' => 'notBlank',
                                'message' => 'Please enter your email.'
                )
            ),

            'password'=>array('Please enter your email.'=>
                            array ( 'rule' => 'notBlank',
                                    'message' => 'Please enter your password.',
                                    'on'        => 'create',
                ),
            'Math password' => array('rule' => 'matchPasswords',
                                     'message' => 'Your password do not match')
            ),

            'password_confirmation'=>array('Please enter your email.'=>
                            array( 'rule' => 'notBlank',
                                    'message' => 'Please enter your password confirmation.'
                )
            ),
            'resetpassword' =>array('Please enter your email.'=>
                                array( 'rule' => 'notBlank',
                                'message' => 'Please enter your password confirmation.'
                )
            )
        );  
        
        public $hasMany = array(
            'Post' => array(
                'className' => 'Post',
                'foreignKey' => 'user_id'
            )
        );

        public $hasAndBelongsToMany  = array(
            'PostComment' => array(
                'className' => 'Post',
                'joinTable' => 'comments',
                'foreignKey' => 'user_id',
                'associationForeignKey' => 'post_id',
            )
        );
        
        public function searchComment ($id)
        {
            $customers = $this->findById($id);
            $postComment = array();

            foreach ($customers['Post'] as $key => $post) {
                $comment = $this->Comment->query('SELECT users.id,comment,name FROM cakeblog.comments join users on users.id = comments.user_id where post_id ='.$post['id'].'');
                $post['comment'] = $comment;
                $postComment[]  = $post; 
            }
            return $postComment;
        }

        public function matchPasswords($data)
        {
            if($data['password'] == $this->data['User']['password_confirmation'])
            {
                return true;
            }
            return false;
        }

        public function beforeSave($options = Array())
        {
            if(isset($this->data['User']['password']))
            {
                $this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);
            }
            return true;
        }
    }
?>