<?php
    class Image extends AppModel 
    {
        public $validate = array
        (
            'name' => array('rule' => 'notBlank')
        );
        public $belongsTo = array(
            'Post' => array(
                'className' => 'Post',
                'foreignKey' => 'post_id'
            )
        );
    }
?>