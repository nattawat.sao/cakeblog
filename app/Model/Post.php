<?php
    class Post extends AppModel 
    {
        public $validate = array
        (
            'title' => array('rule' => 'notBlank'),
            'body' => array( 'rule' => 'notBlank')
            
        );
        public $belongsTo = array(
            'User' => array(
                'className' => 'User',
                'foreignKey' => 'user_id'
            )
        );

        public $hasAndBelongsToMany  = array(
            'UserComment' => array(
                'className' => 'User',
                'joinTable' => 'comments',
                'foreignKey' => 'post_id',
                'associationForeignKey' => 'user_id',
            )
        );

        public $hasMany = array(
            'Image' => array(
                'className' => 'Image',
                'foreignKey' => 'post_id'
            )
        );

    }
?>